<?php
session_start();
    if(!isset($_SESSION['user'])){
        header('location: index.html');
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Escolha</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <h1>Escolha uma das seguintes opções</h1>
    <div class="box-buttons">
        <a class="btn btn-primary btn-lg" href="escolha.php?escolha=flores">Flores</a>
        <a class="btn btn-primary btn-lg" href="escolha.php?escolha=animais">Animais</a>
    </div>
</body>
</html>
